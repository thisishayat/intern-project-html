<?php

/**
 * Created by Assaduzzaman Noor(01682777666) on 8/27/2016.
 */

class Admin {
    
   
    private $db_connect;
    public function __construct() {

            $servername = "localhost";
            $username = "root";
            $password = "123";
            $dbname = "heartdis_db";

            $this->db_connect=  mysqli_connect($servername, $username, $password, $dbname);
            if (!$this->db_connect) {
                die('Connection Fail'.  mysqli_error($this->db_connect));
                
            }
        
    }

    private function test_input($data) {
          $data = trim($data);
          $data = stripslashes($data);
          $data = htmlspecialchars($data);
          return $data;
    }
    
    public function admin_login_check($data) {
        $email=$this->test_input($data['username']);
        $pass=$this->test_input($data['password']);

        $sql="SELECT * FROM user WHERE user_name='$email'  AND password='$pass' ";

        $query_result=mysqli_query($this->db_connect, $sql);

        if ($query_result) {
            $admin_info=mysqli_fetch_assoc($query_result);
            if($admin_info) {
                session_start();
                $_SESSION['admin_name']=$admin_info['username'];
                $_SESSION['admin_id']=$admin_info['user_id'];     
                return 1;
            } else {       
                return 0;
            }
        } else {
            die('Query problem'.  mysqli_error($this->db_connect));
        }
    }


    public function admin_reg($data) {
        
        $username=$this->test_input($data['username']);
        $email=$this->test_input($data['email']);
        $password=$this->test_input($data['password']);
        $phone=$this->test_input($data['phone']);
    
       
            $sql="INSERT INTO user (user_names,user_name,password,user_phone,status) VALUES ('$username','$email', '$password','$phone', '1' )";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }


    public function save_m($data) {
        
        $m_name=$this->test_input($data['m_name']);
        $project_id=$this->test_input($data['project_id']);
    
       
            $sql="INSERT INTO mlist (project_id,m_name,status) VALUES ('$project_id','$m_name',  '1' )";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }

    public function update_m($data) {
        
        $m_name=$this->test_input($data['m_name']);
        $project_id=$this->test_input($data['project_id']);
        $m_id=$this->test_input($data['m_id']);
       
            $sql="UPDATE mlist SET m_name='$m_name' WHERE m_id='$m_id' ";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }
    
    
    public function delete_m($id) {
        
            $sql="DELETE FROM mlist WHERE m_id='$id' ";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }


    public function get_mlist($data) {
     
            $sql="SELECT * from mlist WHERE project_id='$data'";
            $query_result=mysqli_query($this->db_connect, $sql);

            if($query_result) {
                    
                return $query_result;
            } 
            else
                return "Failed !!!";
     
    }
    
    
    
    public function save_c($data) {
        
        $c_name=$this->test_input($data['c_name']);
        $m_id=$this->test_input($data['m_id']);
        $project_id=$this->test_input($data['project_id']);
        $c_volume1=$this->test_input($data['c_volume1']);
        $c_volume2=$this->test_input($data['c_volume2']);
        $c_volume=3.1416*($c_volume1/2)*($c_volume1/2)*$c_volume2;
    
       
            $sql="INSERT INTO C_list (p_id,m_id,c_name,c_volume,status) VALUES ('$project_id','$m_id','$c_name','$c_volume',  '1' )";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }

    public function add_sec($c_id,$m_id,$p_id,$data) {
        
 
        $sec=$this->test_input($data['c_time']);
   
       
            $sql="INSERT INTO sec(c_id,m_id,p_id,sec_time,status) VALUES ('$c_id','$m_id','$p_id','$sec',  '1' )";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }


    public function get_sec_list($project_id,$m_id,$c_id) {
     
            $sql="SELECT * from sec WHERE p_id='$project_id' AND m_id='$m_id' AND c_id='$c_id' ";
            $query_result=mysqli_query($this->db_connect, $sql);

            if($query_result) {
                return $query_result;
            } 
            else
                return "Failed !!!";
     
    }

    public function update_c($data) {
        
        $c_name=$this->test_input($data['c_name']);
        $c_volume=$this->test_input($data['c_volume']);
        $c_id=$this->test_input($data['c_id']);
       
            $sql="UPDATE C_list SET c_name='$c_name' , c_volume='$c_volume'  WHERE c_id='$c_id' ";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }
    
    
    public function delete_c($id) {
        
            $sql="DELETE FROM C_list WHERE c_id='$id' ";
            if(mysqli_query($this->db_connect, $sql)) {   
                $message="1";
                return $message;
            } 
            else
                return "Failed !!!";
     
    }


    public function get_clist($data,$mid) {
     
            $sql="SELECT * from C_list WHERE p_id='$data' AND m_id='$mid'";
            $query_result=mysqli_query($this->db_connect, $sql);

            if($query_result) {
                    
                return $query_result;
            } 
            else
                return "Failed !!!";
     
    }





    public function save_project($data) {
        
        $project_name=$this->test_input($data['p_name']);
        $project_discription=$this->test_input($data['p_discription']);
       
            $sql="INSERT INTO project (project_name,project_discription,status) VALUES ('$project_name', '$project_discription', '1' )";
            if(mysqli_query($this->db_connect, $sql)) {
                $message= $this->db_connect->insert_id;
                return $message;
            } 
            else
                return "Failed !!!";
     
    }


    public function get_projectlist() {
     
            $sql="SELECT * from project";
            $query_result=mysqli_query($this->db_connect, $sql);

            if($query_result) {
                    
                return $query_result;
            } 
            else
                return "Failed !!!";
     
    }




    
    
}







