<?php
   require '../class/admin.php';
   $admin=new Admin();
   $project_id="";
   $did="ADD";
   if(isset($_POST["start"])){
       if($_POST["start"]=="true"){
          $project_id=$admin->save_project($_POST);
       }
   }
   else if(isset($_POST["submit"])){
       if($_POST["submit"]=="ADD"){
          $message=$admin->save_c($_POST);
          $project_id=$_POST["project_id"];
       }
       else if($_POST["submit"]=="Update"){
          $message=$admin->update_c($_POST);
          $project_id=$_POST["project_id"];
       }
       else if($_POST["submit"]=="Delete"){
          $c_id=$_GET["update"];
          $message=$admin->delete_c($c_id);
          $project_id=$_POST["project_id"];
       }
       else if($_POST["submit"]=="ADD SEC"){
          $c_id=$_GET["c_id"];
          $m_id=$_GET["m_id"];
          $p_id=$_GET["project_id"];
          $message=$admin->add_sec($c_id,$m_id,$p_id,$_POST);
       }
       else if($_POST["submit"]=="Add Cylinder"){
          $m_id=$_GET["update"];
          $project_id=$_POST["project_id"];
          $url='Location: http://www.3airline.com/valid/?project_id='.$project_id.'&m_id='.$m_id;
          header($url);
       }
   }

   if(isset($_GET["project_id"])){
      $project_id=$_GET["project_id"];
      $m_id=$_GET["m_id"];
   }
   if(isset($_GET["update"])){
      $c_id=$_GET["update"];
      $m_id=$_GET["m_id"];
      $project_id=$_POST["project_id"];
      $did=$_POST["submit"];
      if($did=="Delete")
         $did="ADD";
      if($did=="Update")
         $did="ADD";
   }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Home</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
       
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            

            
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
          
            <li class="treeview">
              <a href="index.php">
                <i class="fa fa-files-o"></i>
                <span>Start Project</span>
              </a>
             
            </li>


<?php

include "header.php";

?>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="text-align:center">
          <h1 style="text-align:center">
            SIMULATION
            <small>Preview</small>
          </h1>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->

              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Data</h3>
                </div>
                <div class="box-body">
                  <div class="row">
                  
                    <div class="col-xs-6">
                      <input type="text" class="form-control" placeholder="Number Of Manifold" readonly>
                      
                    </div>
                    <div class="col-xs-6">
                      <input type="text" class="form-control" placeholder="Cycle Time" readonly>
                    </div>

                  </div>
                  <br>
                  

                  <?php
                       
                       $result=$admin->get_clist($project_id,$m_id);
                        
                       while($row=mysqli_fetch_assoc($result)){
                           if($did=="Edit"){
                              if($c_id==$row[c_id]){
                                 
                                 $mm_name=$row[c_name];
                                 $mm_v=$row[c_volume];

                              }
                           }

                           echo '

<div class="row">

             <form method="POST" action="?project_id='.$project_id.'&m_id='.$m_id.'&update='.$row[c_id].'">

                    <div class="col-xs-3">
                      <input type="text" name="c_name" class="form-control" value="'.$row[c_name].'" readonly>
                      <input type="hidden" name="project_id" value="'.$project_id.'">
                    </div>
                    <div class="col-xs-3">
                      <input type="submit" name="submit"  class="form-control" value="'.$row[c_volume].'" readonly>
                    </div>
   
                    <div class="col-xs-3">
                      <input type="submit" name="submit"  class="form-control btn btn-warning" value="Edit">
                    </div>
                    <div class="col-xs-3">
                      <input type="submit" name="submit"  class="form-control btn btn-danger" value="Delete">
                    </div>

                  
             </form>

                  </div>

      <div class="row" style="padding-top:5px;">

             <form method="POST" action="?project_id='.$project_id.'&m_id='.$m_id.'&c_id='.$row[c_id].'">';


             $result4=$admin->get_sec_list($project_id,$m_id,$row[c_id]);
                        
              while($row4=mysqli_fetch_assoc($result4)){
echo '
                    <div class="col-xs-1">
                             <span class="btn btn-default">'.$row4["sec_time"].'</span>
                    </div>
';

              }
echo '
                  <div class="col-xs-4">
                     <input type="number" name="c_time" class="form-control"   style="width:40%;float:left">
                     <input type="submit" name="submit"  class="form-control btn btn-info " value="ADD SEC"style="width:60%">
                   </div>

                  
             </form>

                  </div>


 <br>';

                       }       
                 

                  ?>
              






                  <div class="row">

             <form method="POST" action="">

                                        
                    <?php

                     if($did=="ADD"){
                         echo '
                    <div class="col-xs-3">
                      <input type="text" name="c_name" class="form-control" placeholder="Cylinder Name" required>
                      <input type="hidden" name="project_id" value="'.$project_id.'">
                      <input type="hidden" name="m_id" value="'.$m_id.'">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" name="c_volume1" class="form-control" placeholder="Bore" required>
                      <input type="hidden" name="project_id" value="'.$project_id.'">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" name="c_volume2" class="form-control" placeholder="Stroke" required>
                    </div>
                    <div class="col-xs-3">
                      <input type="submit" name="submit"  class="form-control btn btn-success" value="ADD">
                    </div>';
                     }
                     else if($did=="Edit"){
                         echo '
                    <div class="col-xs-3">
                      <input type="text" name="c_name" class="form-control" value="'.$mm_name.'">
                      <input type="hidden" name="project_id" value="'.$project_id.'">
                      <input type="hidden" name="m_id" value="'.$m_id.'">
                      <input type="hidden" name="c_id" value="'.$c_id.'">
                    </div>
                    <div class="col-xs-4">
                      <input type="text" name="c_volume" class="form-control" value="'.$mm_v.'">
                  
                    </div>
                
                    <div class="col-xs-4">
                      <input type="submit" name="submit"  class="form-control btn btn-success" value="Update">
                    </div>';
                     }
                    

                    ?>

             </form>

                  </div>

                </div><!-- /.box-body -->

              </div><!-- /.box -->

     

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

                
            <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">TIME VS AIR-FLOW GRAPH
</h3>
                  <div class="box-tools pull-right">
                    Real time
                    <div class="btn-group" id="realtime" data-toggle="btn-toggle">
                      <button type="button" class="btn btn-default btn-xs active" data-toggle="on">On</button>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="off">Off</button>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div id="interactive" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="816" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 816px; height: 300px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 20px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 96px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 176px; text-align: center;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 255px; text-align: center;">30</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 334px; text-align: center;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 413px; text-align: center;">50</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 493px; text-align: center;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 572px; text-align: center;">70</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 651px; text-align: center;">80</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 147px; top: 283px; left: 731px; text-align: center;">90</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 270px; left: 12px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 216px; left: 6px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 162px; left: 6px; text-align: right;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 108px; left: 6px; text-align: right;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 54px; left: 6px; text-align: right;">80</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 1px; text-align: right;">100</div></div></div><canvas class="flot-overlay" width="816" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 816px; height: 300px;"></canvas></div>
                </div><!-- /.box-body-->
              </div>


   
    
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.1.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://asd-soft.com">ASD-SOFT</a>.</strong> All rights reserved.
      </footer>

      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>





    <!-- FLOT CHARTS -->
    <script src="plugins/flot/jquery.flot.min.js"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="plugins/flot/jquery.flot.resize.min.js"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="plugins/flot/jquery.flot.pie.min.js"></script>
    <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
    <script src="plugins/flot/jquery.flot.categories.min.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        /*
         * Flot Interactive Chart
         * -----------------------
         */
        // We use an inline data source in the example, usually data would
        // be fetched from a server
        var data = [], totalPoints = 100;
        function getRandomData() {

          if (data.length > 0)
            data = data.slice(1);

          // Do a random walk
          while (data.length < totalPoints) {

            var prev = data.length > 0 ? data[data.length - 1] : 50,
                    y = prev + Math.random() * 10 - 5;

            if (y < 0) {
              y = 0;
            } else if (y > 100) {
              y = 100;
            }

            data.push(y);
          }

          // Zip the generated y values with the x values
          var res = [];
          for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]]);
          }

          return res;
        }

        var interactive_plot = $.plot("#interactive", [getRandomData()], {
          grid: {
            borderColor: "#f3f3f3",
            borderWidth: 1,
            tickColor: "#f3f3f3"
          },
          series: {
            shadowSize: 0, // Drawing is faster without shadows
            color: "#3c8dbc"
          },
          lines: {
            fill: true, //Converts the line chart to area chart
            color: "#3c8dbc"
          },
          yaxis: {
            min: 0,
            max: 100,
            show: true
          },
          xaxis: {
            show: true
          }
        });

        var updateInterval = 500; //Fetch data ever x milliseconds
        var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching
        function update() {

          interactive_plot.setData([getRandomData()]);

          // Since the axes don't change, we don't need to call plot.setupGrid()
          interactive_plot.draw();
          if (realtime === "on")
            setTimeout(update, updateInterval);
        }

        //INITIALIZE REALTIME DATA FETCHING
        if (realtime === "on") {
          update();
        }
        //REALTIME TOGGLE
        $("#realtime .btn").click(function () {
          if ($(this).data("toggle") === "on") {
            realtime = "on";
          }
          else {
            realtime = "off";
          }
          update();
        });
        /*
         * END INTERACTIVE CHART
         */


        /*
         * LINE CHART
         * ----------
         */
        //LINE randomly generated data

        var sin = [], cos = [];
        for (var i = 0; i < 14; i += 0.5) {
          sin.push([i, Math.sin(i)]);
          cos.push([i, Math.cos(i)]);
        }
        var line_data1 = {
          data: sin,
          color: "#3c8dbc"
        };
        var line_data2 = {
          data: cos,
          color: "#00c0ef"
        };
        $.plot("#line-chart", [line_data1, line_data2], {
          grid: {
            hoverable: true,
            borderColor: "#f3f3f3",
            borderWidth: 1,
            tickColor: "#f3f3f3"
          },
          series: {
            shadowSize: 0,
            lines: {
              show: true
            },
            points: {
              show: true
            }
          },
          lines: {
            fill: false,
            color: ["#3c8dbc", "#f56954"]
          },
          yaxis: {
            show: true,
          },
          xaxis: {
            show: true
          }
        });
        //Initialize tooltip on hover
        $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
          position: "absolute",
          display: "none",
          opacity: 0.8
        }).appendTo("body");
        $("#line-chart").bind("plothover", function (event, pos, item) {

          if (item) {
            var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

            $("#line-chart-tooltip").html(item.series.label + " of " + x + " = " + y)
                    .css({top: item.pageY + 5, left: item.pageX + 5})
                    .fadeIn(200);
          } else {
            $("#line-chart-tooltip").hide();
          }

        });
        /* END LINE CHART */

        /*
         * FULL WIDTH STATIC AREA CHART
         * -----------------
         */
        var areaData = [[2, 88.0], [3, 93.3], [4, 102.0], [5, 108.5], [6, 115.7], [7, 115.6],
          [8, 124.6], [9, 130.3], [10, 134.3], [11, 141.4], [12, 146.5], [13, 151.7], [14, 159.9],
          [15, 165.4], [16, 167.8], [17, 168.7], [18, 169.5], [19, 168.0]];
        $.plot("#area-chart", [areaData], {
          grid: {
            borderWidth: 0
          },
          series: {
            shadowSize: 0, // Drawing is faster without shadows
            color: "#00c0ef"
          },
          lines: {
            fill: true //Converts the line chart to area chart
          },
          yaxis: {
            show: false
          },
          xaxis: {
            show: false
          }
        });

        /* END AREA CHART */

        /*
         * BAR CHART
         * ---------
         */

        var bar_data = {
          data: [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]],
          color: "#3c8dbc"
        };
        $.plot("#bar-chart", [bar_data], {
          grid: {
            borderWidth: 1,
            borderColor: "#f3f3f3",
            tickColor: "#f3f3f3"
          },
          series: {
            bars: {
              show: true,
              barWidth: 0.5,
              align: "center"
            }
          },
          xaxis: {
            mode: "categories",
            tickLength: 0
          }
        });
        /* END BAR CHART */

        /*
         * DONUT CHART
         * -----------
         */

        var donutData = [
          {label: "Series2", data: 30, color: "#3c8dbc"},
          {label: "Series3", data: 20, color: "#0073b7"},
          {label: "Series4", data: 50, color: "#00c0ef"}
        ];
        $.plot("#donut-chart", donutData, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        /*
         * END DONUT CHART
         */

      });

      /*
       * Custom Label formatter
       * ----------------------
       */
      function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + "<br>"
                + Math.round(series.percent) + "%</div>";
      }
    </script>

  </body>
</html>

